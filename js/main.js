
// 1. dupliranje
// Duplira logoList, duplira i top-logo i u kloniranju zadrzi clasu, te se vise ne mijenja, tako da ostaju dva ista koja se ne mijenjaju
// Takodje duplira nekad dva ista random, te budu jedan kraj drugog
// Cak mi se desilo u testiranju da su u jednom momentu bila 3 ista

// 2. kloniranje
// Na prvi load je dobro, kad krene random, ubaci <li> inside <li>
// Ja sam pokusavao ovo popraiti, skontao sam da sa replaceWith (line 113) ne ubaci <li> u <li> ali onda animacija ne moze


let logoList = document.querySelector('.full-list li');
let randomList = document.getElementsByClassName('ul.random-list');

(function ($) {

    var logoList = document.getElementsByClassName('.full-list');
    let $logoFullList = $('.full-list');
    let $logoFullListItems = $('.full-list li');

    let $logoRandomList = $('.random-list');

    // Hide full list at first
    $($logoFullList).hide();

    var showRandom = false;
    let shouldChangeSingleLogo = true;

    // show random logos
    showLogosRandomly();

    getTopLogoItems();


    let $logoButton = $('#clients-btn');

    $logoButton.click(() => {

        if (showRandom) {
            showLogosRandomly();
        } else {
            showLogosAlpabethicaly()
        }
        showRandom = !showRandom;
        $logoButton.text($logoButton.text() == '> Alle Kunden einblenden' ? '> Kunden ausblenden' : '> Alle Kunden einblenden');
    })


    function showLogosAlpabethicaly() {
        shouldChangeSingleLogo = false;

        $('.full-list li').sort(sort_li).appendTo(".full-list");
        function sort_li(a, b) {
            return ($(b).data('title')) < ($(a).data('title')) ? 1 : -1;
        }
        $($logoRandomList).hide();
        $($logoFullList).fadeIn();

    }


    setInterval(() => {
        if (shouldChangeSingleLogo) {
            changeSingleLogo();
        }
    }, 5000);

    function showLogosRandomly() {

        shouldChangeSingleLogo = true;

        if ($logoRandomList.length > 0) {
            $logoRandomList.empty();
        }

        let topLogos = getTopLogoItems();

        let topLogosCount = topLogos.length ?? 0;

        let maxNumber = 4;
        if (topLogos.length > 0) {
            maxNumber = 4 - topLogos.length;

        }

        for (let i = topLogos.length; i >= 0; i--) {
            var rnd = Math.floor(Math.random() * $logoFullListItems.length);
            $(topLogos[i]).clone().appendTo('.random-list');
        }

        for (let i = maxNumber; i >= 0; i--) {
            var rnd = Math.floor(Math.random() * $logoFullListItems.length);
            $($logoFullListItems[rnd]).clone().appendTo('.random-list');
        }

        $($logoFullList).hide();
        $($logoRandomList).fadeIn();
    }



    function getTopLogoItems() {
        return $('.full-list').children('.top-logo');
    }


    function changeSingleLogo() {
        var $randomLogos = $('.random-list').children('li').not('.top-logo');
        var rnd = Math.floor(Math.random() * $randomLogos.length);
        const element = $randomLogos[rnd];

        $(element).fadeOut('slow', function () {

            let randomLogo = getRandomLogo();
            $img = $(randomLogo).find('a');
            $title = $(randomLogo).data('title');
            $(this).html($img);
            $(this).attr('data-title', $title);
            $(element).fadeIn('slow');
        })
    }

    function getRandomLogo() {
        let $logoFullListItemsNotTopLogo = $('.full-list li').not('.top-logo');
        var rnd = Math.floor(Math.random() * $logoFullListItemsNotTopLogo.length);
        return $($logoFullListItemsNotTopLogo[rnd]).clone();
    }


})(jQuery)

